import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:openchat/pages/login_page.dart';
import 'package:openchat/pages/registration_page.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage>
    with SingleTickerProviderStateMixin {
  AnimationController? animationController;
  Animation? animation;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
      // lowerBound: 0,
      // upperBound: 100,
    );
    animation = ColorTween(begin: Colors.orange, end: Colors.white)
        .animate(animationController!);

    animationController!.forward();

    animationController!.addListener(() {
      // print(animationController!.value);
      // if (animationController!.value == 100) {
      //   animationController!.reverse();
      // }
      // if (animationController!.value == 1) {
      //   animationController!.forward();
      // }
      setState(() {});
    });

    requestPermission();
    setupInteractedMessage();
  }

  //FIREBASE CLOUD MESSAGING FCM

  void requestPermission() async {
    try {
      NotificationSettings setting = await FirebaseMessaging.instance
          .requestPermission(alert: true, badge: true, sound: true);

      if (setting.authorizationStatus == AuthorizationStatus.authorized) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('User granted permission'),
          ),
        );
      } else if (setting.authorizationStatus ==
          AuthorizationStatus.provisional) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('User granted provisional permission'),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('User not granted /  declined permission'),
          ),
        );
      }
    } catch (error) {
      print(error.toString());
    }
  }


  setupInteractedMessage(){

    //foreground
    FirebaseMessaging.onMessage.listen((event) {
      print(event.notification!.title??'');
      print(event.notification!.body??'');
    });

  }





  @override
  void dispose() {
    super.dispose();
    animationController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation!.value,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Hero(
              tag: 'open-chat-logo',
              child: Image.asset(
                'assets/images/logo.png',
                height: animationController!.value * 100,
              ),
            ),
            Center(
              child: Text(
                'Open-Chat',
                // '${animationController!.value.toInt()}%',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 45,
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
                decoration: BoxDecoration(
                  color: Colors.lightBlueAccent,
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Text(
                  'LOGIN',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RegistrationPage()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
                decoration: BoxDecoration(
                  color: Colors.lightBlueAccent,
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Text(
                  'Registration',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
