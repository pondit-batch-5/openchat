import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

FirebaseFirestore? firebaseFirestore;
FirebaseAuth? auth;

class ChatPage extends StatefulWidget {
  const ChatPage({super.key});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  TextEditingController? messageTxtController;
  FirebaseStorage? storage;

  @override
  void initState() {
    super.initState();
    messageTxtController = TextEditingController();
    firebaseFirestore = FirebaseFirestore.instance;
    auth = FirebaseAuth.instance;
    storage = FirebaseStorage.instance;
  }

  void saveMessage(String url) {
    firebaseFirestore!.collection('pondit_message').add({
      'sender': auth!.currentUser?.email,
      'text': messageTxtController!.text,
      'timestamp': DateTime.now().microsecondsSinceEpoch,
      'image_url': url,
    });
  }

  void getImageFromCameraOrGallery(ImageSource source) async {
    final XFile? xFile = await ImagePicker().pickImage(source: source);
    String fileName = 'pondit_${DateTime.now().microsecondsSinceEpoch}.jpg';
    Reference reference = storage!.ref().child('pondit_photos').child(fileName);
    uploadFileInFireBaseStorage(xFile!, reference);
  }

  uploadFileInFireBaseStorage(XFile xFile, Reference ref) async {
    await ref.putFile(File(xFile.path));
    downloadUrlFromFireBaseStorage(ref);
  }

  downloadUrlFromFireBaseStorage(Reference ref) async {
    String url = await ref.getDownloadURL();
    saveMessage(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.close),
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MessageStream(),
            Container(
              decoration: BoxDecoration(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 32,
                    child: IconButton(
                      onPressed: () {
                        getImageFromCameraOrGallery(ImageSource.gallery);
                      },
                      icon: Icon(
                        Icons.image,
                        color: Colors.blue,
                        size: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 32,
                    child: IconButton(
                      onPressed: () {
                        getImageFromCameraOrGallery(ImageSource.camera);
                      },
                      icon: Icon(
                        Icons.camera_alt,
                        color: Colors.blue,
                        size: 20,
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: messageTxtController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          border: InputBorder.none,
                          hintText: 'Type your message here...'),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      saveMessage('empty');
                      messageTxtController!.clear();
                    },
                    child: Text('Send'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  const MessageStream({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: firebaseFirestore!
          .collection('pondit_message')
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        List<MessageBubble> messageList = [];
        for (QueryDocumentSnapshot queryDocumentSnapshot
            in snapshot.data!.docs) {
          String mMessage = queryDocumentSnapshot.get('text');
          String mSender = queryDocumentSnapshot.get('sender');
          String mImageUrl = queryDocumentSnapshot.get('image_url');
          MessageBubble messageBubble = MessageBubble(
            mText: mMessage,
            mSender: mSender,
            imageUrl: mImageUrl,
            isUserMe: auth!.currentUser!.email == mSender,
          );

          messageList.add(messageBubble);
        }

        return Expanded(
          child: ListView(
            reverse: true,
            children: messageList,
          ),
        );
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String? mText;
  final String? mSender;
  final String? imageUrl;
  final bool? isUserMe;

  const MessageBubble(
      {super.key, this.mText, this.mSender, this.imageUrl, this.isUserMe});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: isUserMe == true
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          Text(
            mSender ?? '',
            style: TextStyle(
              fontSize: 12,
              color: Colors.white,
            ),
          ),
          imageUrl == 'empty'
              ? Material(
                  color: isUserMe == true ? Colors.blue : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                    topLeft: isUserMe == true
                        ? Radius.circular(50)
                        : Radius.circular(0),
                    topRight: isUserMe == true
                        ? Radius.circular(0)
                        : Radius.circular(50),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    child: Text(
                      mText ?? '',
                      style: TextStyle(
                          fontSize: 15,
                          color: isUserMe == true ? Colors.white : Colors.blue),
                    ),
                  ),
                )
              : Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.network(
                    imageUrl ?? '',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                ),
        ],
      ),
    );
  }
}
