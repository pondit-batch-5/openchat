import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:openchat/pages/welcome_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Open Chat',
      theme: ThemeData.dark(),
      home: const WelcomePage(),
    );
  }
}
